#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <time.h>
#include <netinet/tcp.h>

#include "uftpd.h"
#include "cmd.h"

static void start_transfer(struct connection *conn)
{
	bufferevent_enable(conn->data.bufev, EV_WRITE);

	if (conn->data.filebuf != NULL)
	{
		/* We are transferring a file. */
		char line[1024];

		INFO_OUT("starting file transfer\n");
		snprintf(line, 1024, "150 Opening BINARY mode data connection for ftp.c (%ld bytes).\r\n", conn->size);
		send_response(conn->ctrl.bufev, line);

		int true = 1, false = 0;
		int fd = bufferevent_getfd(conn->data.bufev);

		if (setsockopt(fd, IPPROTO_TCP, TCP_CORK, &true, sizeof(true)) < 0)
			perror("setsockopt");
	}
	else
	{
		/* We are transferring a file list. */

		INFO_OUT("starting file list transfer\n");
		send_response(conn->ctrl.bufev, "150 Here comes the directory listing.\r\n");
	}
}

static int send_file_part(struct connection *conn)
{
	evbuffer_remove_buffer(conn->data.filebuf, conn->data.buf, COPY_SIZE);
	return evbuffer_get_length(conn->data.filebuf) == 0;
}

static char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                         "Sep", "Oct", "Nov", "Dec"};

static int send_listing_line(struct connection *conn)
{
	if (conn->namelist != NULL && conn->curent < conn->nument)
	{
		struct stat buf;
		char mode_str[11] = "??????????",
		     time_str[13] = "Unknown";
		struct tm tm;
		memset(&buf, '\0', sizeof(struct stat));

		chdir(conn->listdir);

		if (lstat(conn->namelist[conn->curent]->d_name, &buf) == 0)
		{
			gmtime_r(&buf.st_mtime, &tm);

			mode_str[ 0] = S_ISDIR(buf.st_mode)  ? 'd' : '-';
			mode_str[ 1] = buf.st_mode & S_IRUSR ? 'r' : '-';
			mode_str[ 2] = buf.st_mode & S_IWUSR ? 'w' : '-';
			mode_str[ 3] = buf.st_mode & S_IXUSR ? 'x' : '-';
			mode_str[ 4] = buf.st_mode & S_IRGRP ? 'r' : '-';
			mode_str[ 5] = buf.st_mode & S_IWGRP ? 'w' : '-';
			mode_str[ 6] = buf.st_mode & S_IXGRP ? 'x' : '-';
			mode_str[ 7] = buf.st_mode & S_IROTH ? 'r' : '-';
			mode_str[ 8] = buf.st_mode & S_IWOTH ? 'w' : '-';
			mode_str[ 9] = buf.st_mode & S_IXOTH ? 'x' : '-';
			mode_str[10] = '\0';

			/* TODO: Show time instead of year if year is current year. */
			snprintf(time_str, 13, "%s %2d %4d",
				months[tm.tm_mon], tm.tm_mday, tm.tm_year + 1900);
		}
		else
		{
			ERROR_OUT("could not stat file %s\n",
				conn->namelist[conn->curent]->d_name);
		}

		char line[1024];

		snprintf(line, 1024, "%s %3d %-8d %-8d %10lu %12s %s\r\n",
			mode_str,
			buf.st_nlink,
			buf.st_uid,
			buf.st_gid,
			buf.st_size,
			time_str,
			conn->namelist[conn->curent]->d_name);

		send_response(conn->data.bufev, line);
		free(conn->namelist[conn->curent]);
		conn->curent++;

		return 0;
	}
	else if (conn->namelist != NULL)
	{
		free(conn->namelist);
		conn->namelist = NULL;
		conn->nument = 0;
		conn->curent = 0;

		INFO_OUT("directory listing done\n");

		return 1;
	}

	return 1;
}

static void cb_data_event(struct bufferevent *bufev, short what, void *ctx)
{
	struct connection *conn = ctx;

	INFO_OUT("data connection event %d\n", what);

	if (what & BEV_EVENT_EOF || what & BEV_EVENT_TIMEOUT)
	{
		if (what & BEV_EVENT_TIMEOUT)
		{
			INFO_OUT("connection timed out\n");
			send_response(conn->ctrl.bufev, "421 Timeout.\r\n");
		}
		if (what & BEV_EVENT_EOF)
			INFO_OUT("connection closed\n");

		if (!conn->data.done)
			send_response(conn->ctrl.bufev, "426 Data connection closed; aborting transfer.\r\n");

		close_data(conn);
	}
}

static void cb_data_write(struct bufferevent *bufev, void *ctx)
{
	struct connection *conn = ctx;
	int done = conn->data.done;

	if (!done)
	{
		if (conn->data.filebuf != NULL)
			done = send_file_part(conn);
		else if (conn->namelist != NULL)
			done = send_listing_line(conn);
	}

	if (done && evbuffer_get_length(conn->data.buf) == 0)
	{	
		INFO_OUT("transfer done\n");
		close_data(conn);

		send_response(conn->ctrl.bufev, "226 Transfer OK.\r\n");
	}
}

static void cb_data_connect(struct evconnlistener *listener, evutil_socket_t sock, struct sockaddr *addr, int len, void *ptr)
{
	struct connection *conn = ptr;

	INFO_OUT("incoming data connection\n");

	if (conn->data.bufev != NULL)
	{
		ERROR_OUT("we already had a data connection, closing connection");
		free_connection(conn);
	}

	conn->data.bufev = bufferevent_socket_new(conn->base, sock, BEV_OPT_CLOSE_ON_FREE);
	conn->data.buf = bufferevent_get_output(conn->data.bufev);
	conn->data.done = 0;

	bufferevent_setcb(conn->data.bufev, NULL, cb_data_write, cb_data_event, conn);
	evconnlistener_free(conn->listener);
	conn->listener = NULL;

	if (conn->data.filebuf != NULL || conn->namelist != NULL)
		start_transfer(conn);

	INFO_OUT("closed listener\n");
}

void cmd_retr(struct connection *conn, const char *path)
{
	INFO_OUT("RETR %s\n", path);

	int fd;
	struct stat statbuf;

	chdir(conn->cwd);

	if ((fd = open(path, O_RDONLY)) >= 0 && fstat(fd, &statbuf) == 0)
	{
		char line[1024];

		INFO_OUT("about to open file\n");

		conn->data.filebuf = evbuffer_new();
		conn->size = statbuf.st_size;
		evbuffer_add_file(conn->data.filebuf, fd, 0, statbuf.st_size);

		INFO_OUT("opened file for reading, size %ld bytes\n", statbuf.st_size);
	}
	else
	{
		ERROR_OUT("could not open file for reading\n");
		send_response(conn->ctrl.bufev, "550 Could not open file for reading.\r\n");
		close_data(conn);
	}

	if (conn->data.bufev != NULL)
		start_transfer(conn);
}

void cmd_user(struct connection *conn, const char *username)
{
	INFO_OUT("USER %s\n", username);
	send_response(conn->ctrl.bufev, "230 User logged in, proceed.\r\n");
}

void cmd_syst(struct connection *conn)
{
	INFO_OUT("SYST\n");
	send_response(conn->ctrl.bufev, "215 UNIX Type: L8\r\n");
}

void cmd_port(struct connection *conn, const char *options)
{
	INFO_OUT("PORT %s\n", options);
	send_response(conn->ctrl.bufev, "502 Active mode not allowed.\r\n");
}

void cmd_type(struct connection *conn, const char *type)
{
	INFO_OUT("TYPE %s\n", type);
	send_response(conn->ctrl.bufev, "200 Switching to Binary mode.\r\n");
}

void cmd_pwd(struct connection *conn)
{
	INFO_OUT("PWD\n");

	send_response(conn->ctrl.bufev, "257 \"");
	send_response(conn->ctrl.bufev, conn->cwd);
	send_response(conn->ctrl.bufev, "\"\r\n");
}

void cmd_pasv(struct connection *conn)
{
	char response[1024+1];
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(struct sockaddr_in);
	unsigned char *ip;
	unsigned short port;
	struct hostent *hostent;

	INFO_OUT("PASV\n");

	struct sockaddr_in sockaddr;

	if (conn->listener != NULL)
	{
		ERROR_OUT("we are already listening, closing\n");
		evconnlistener_free(conn->listener);
	}

	memset(&sockaddr, '\0', sizeof(struct sockaddr_in));
	sockaddr.sin_family = AF_INET,
	sockaddr.sin_addr.s_addr = INADDR_ANY,

	conn->listener = evconnlistener_new_bind(conn->base, cb_data_connect, conn, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, BACKLOG, (struct sockaddr *)&sockaddr, sizeof(struct sockaddr_in));

	ip = (unsigned char *)&local_addr.s_addr;

	getsockname(evconnlistener_get_fd(conn->listener), (struct sockaddr *)&addr, &addrlen);
	port = ntohs(addr.sin_port);

	INFO_OUT("started listening for data connections on port %d\n", port);

	snprintf(response, 1024, "227 Entering Passive Mode (%hhu,%hhu,%hhu,%hhu,%d,%d).\r\n",
			ip[0],
			ip[1],
			ip[2],
			ip[3],
			port / 256,
			port % 256
		);

	bufferevent_write(conn->ctrl.bufev, response, strlen(response));
}

void cmd_feat(struct connection *conn)
{
	INFO_OUT("FEAT\n");
	send_response(conn->ctrl.bufev, "211 No RFC 959 features supported.\r\n");
}

void cmd_cwd(struct connection *conn, const char *path)
{
	INFO_OUT("CWD %s\n", path);

	struct stat buf;

	chdir(conn->cwd);

	if (stat(path, &buf) == 0 && realpath(path, conn->cwd) != NULL)
		send_response(conn->ctrl.bufev, "250 Directory successfully changed.\r\n");
	else
		send_response(conn->ctrl.bufev, "550 Failed to change directory.\r\n");
}

void cmd_list(struct connection *conn, const char *path)
{
	struct stat buf;

	INFO_OUT("LIST %s\n", path);

	if (conn->listener == NULL && conn->data.bufev == NULL)
	{
		ERROR_OUT("list without data connection\n");
		send_response(conn->ctrl.bufev, "425 Use PASV first.\r\n");
		return;
	}

	if (path == NULL)
	{
		strncpy(conn->listdir, conn->cwd, PATH_MAX);
		conn->nument = scandir(conn->cwd, &conn->namelist, 0, alphasort);
	}
	else
	{
		chdir(conn->cwd);

		if (stat(path, &buf) != 0 || realpath(path, conn->listdir) == NULL)
		{
			ERROR_OUT("could not open directory\n");
			send_response(conn->ctrl.bufev, "550 Could not read current working directory.\r\n");
			return;
		}

		INFO_OUT("listing %s\n", conn->listdir);

		conn->nument = scandir(conn->listdir, &conn->namelist, 0, alphasort);
	}

	conn->curent = 0;

	if (conn->nument < 0)
	{
		ERROR_OUT("could not open directory\n");
		send_response(conn->ctrl.bufev, "550 Could not read current working directory.\r\n");
		close_data(conn);
		return;
	}

	if (conn->data.bufev != NULL)
	{
		INFO_OUT("got list, all we need for a transfer is now available\n");
		start_transfer(conn);
	}
}

void cmd_quit(struct connection *conn)
{
	INFO_OUT("QUIT\n");

	send_response(conn->ctrl.bufev, "221 Good bye.\r\n");
	conn->ctrl.done = 1;
}

void cmd_pass(struct connection *conn, const char *password)
{
	INFO_OUT("PASS ****\n");

	send_response(conn->ctrl.bufev, "202 Command not implemented.\r\n");
}

void cmd_site(struct connection *conn, const char *command)
{
	INFO_OUT("SITE %s\n", command);

	send_response(conn->ctrl.bufev, "202 Command not implemented.\r\n");
}

void cmd_abor(struct connection *conn)
{
	INFO_OUT("ABOR\n");

	close_data(conn);

	send_response(conn->ctrl.bufev, "226 Transfer aborted.\r\n");
}

void cmd_noop(struct connection *conn)
{
	INFO_OUT("NOOP\n");

	send_response(conn->ctrl.bufev, "200 Derp.\r\n");
}

void cmd_unkn(struct connection *conn)
{
	INFO_OUT("???\n");

	send_response(conn->ctrl.bufev, "500 Command not recognized.\r\n");
}