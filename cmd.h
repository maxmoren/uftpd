#ifndef __CMD_H
#define __CMD_H

void cmd_abor(struct connection *);
void cmd_cwd (struct connection *, const char *);
void cmd_feat(struct connection *);
void cmd_list(struct connection *, const char *);
void cmd_noop(struct connection *);
void cmd_pass(struct connection *, const char *);
void cmd_pasv(struct connection *);
void cmd_port(struct connection *, const char *);
void cmd_pwd (struct connection *);
void cmd_retr(struct connection *, const char *);
void cmd_site(struct connection *, const char *);
void cmd_syst(struct connection *);
void cmd_type(struct connection *, const char *);
void cmd_user(struct connection *, const char *);
void cmd_quit(struct connection *);

#endif
