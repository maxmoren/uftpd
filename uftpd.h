#ifndef __UFTPD_H
#define __UFTPD_H

#include <limits.h>
#include <dirent.h>
#include <netinet/in.h>

#define BUFLEN 4096
#define COPY_SIZE 524288 /* 512 KiB */
#define BACKLOG 8

struct parser
{
	int cs;
	char buf[BUFLEN+1];
	int buflen;
};

struct connection
{
	struct event_base *base;
	struct parser parser;
	char cwd[PATH_MAX + 1];
	char listdir[PATH_MAX + 1];

	struct
	{
		struct bufferevent *bufev;
		struct evbuffer *buf;
		struct evbuffer *filebuf;
		int done;

	} data;

	struct
	{
		struct bufferevent *bufev;
		struct evbuffer *buf;
		int done;

	} ctrl;

	struct dirent **namelist;
	int nument, curent;
	off_t size;

	struct evconnlistener *listener;
};

extern struct in_addr local_addr;

void send_response(struct bufferevent *, const char *);
void init_parser(struct connection *);
void parse(struct connection *, const char *, size_t);
void close_data(struct connection *);
void free_connection(struct connection *);

#define INFO_OUT(format, ...) \
	printf("%7s:%-3d -- %s(): " format, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);

#define ERROR_OUT(format, ...) \
	fprintf(stderr, "%7s:%-3d -- %s():\e[31m " format "\e[0m", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);

#endif
