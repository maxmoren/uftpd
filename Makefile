TARGET = uftpd
CC     = gcc
CFLAGS = -ggdb -W
LIBS   = -levent_core
SRCS   = ftp.c uftpd.c cmd.c
RAGEL  = ragel

MAKEDEPEND = $(CC) -M $(CFLAGS) -o $*.d $<

all: Makefile $(TARGET)
	
$(TARGET): $(SRCS:.c=.o)
	$(CC) $(SRCS:.c=.o) -o $(TARGET) $(LIBS)

ftp.c: ftp.rl
	$(RAGEL) $<

%.o: %.d

%.d: %.c
	$(MAKEDEPEND)

clean:
	$(RM) $(TARGET) $(SRCS:.c=.o) $(SRCS:.c=.d)

-include $(SRCS:.c=.d)
