#include <stdio.h>
#include <stdlib.h>

#include "uftpd.h"
#include "cmd.h"

%%{
	machine ftpparse;
	access conn->parser.;

	action append {
		if (conn->parser.buflen < BUFLEN)
			conn->parser.buf[conn->parser.buflen++] = fc;
	}

	action term {
		if (conn->parser.buflen < BUFLEN)
			conn->parser.buf[conn->parser.buflen++] = '\0';
	}

	action clear { conn->parser.buflen = 0; }

	action unkn {
		cmd_unkn(conn);
		while (p < pe && *p != '\n')
			++p;
		fgoto main;
	}

	action list { cmd_list(conn, NULL); }
	action list2 { cmd_list(conn, conn->parser.buf); }
	action user { cmd_user(conn, conn->parser.buf); }
	action pass { cmd_pass(conn, conn->parser.buf); }
	action cwd  { cmd_cwd(conn, conn->parser.buf); }
	action site { cmd_site(conn, conn->parser.buf); }
	action retr { cmd_retr(conn, conn->parser.buf); }
	action pwd  { cmd_pwd(conn); }
	action pasv { cmd_pasv(conn); }
	action port { cmd_port(conn, conn->parser.buf); }
	action syst { cmd_syst(conn); }
	action quit { cmd_quit(conn); }
	action type { cmd_type(conn, conn->parser.buf); }
	action feat { cmd_feat(conn); }
	action abor { cmd_abor(conn); }
	action cdup { cmd_cwd(conn, ".."); }
	action noop { cmd_noop(conn); }

	string = [^\r\n]+ >clear $append %term;

	nl = ('\n' | '\r\n');

	command = 'LIST'            nl @list
	        | 'LIST' ' ' string nl @list2
	        | 'USER' ' ' string nl @user
	        | 'PASS' ' ' string nl @pass
	        | 'CWD'  ' ' string nl @cwd
	        | 'PWD'             nl @pwd
	        | 'PASV'            nl @pasv
	        | 'FEAT'            nl @feat
	        | 'SITE' [^\r\n]*   nl @site
	        | 'PORT' ' ' string nl @port
	        | 'SYST'            nl @syst
	        | 'QUIT'            nl @quit
	        | 'TYPE' ' ' string nl @type
	        | 'RETR' ' ' string nl @retr
	        | 'ABOR'            nl @abor
	        | 'CDUP'            nl @cdup
	        | 'NOOP'            nl @noop;

	line = command $! unkn;

	main := line*;
}%%

%% write data;

void init_parser(struct connection *conn)
{
	%% write init;
}

void parse(struct connection *conn, const char *data, size_t len)
{
	const char *p = data, *pe = data + len, *eof = NULL;

	%% write exec;
}
