#include <event.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "uftpd.h"

struct in_addr local_addr;
static char cwd[PATH_MAX];

void send_response(struct bufferevent *bufev, const char *response)
{
	bufferevent_write(bufev, response, strlen(response));
}

void close_data(struct connection *conn)
{
	if (conn->data.bufev != NULL)
	{
		bufferevent_free(conn->data.bufev);
		conn->data.bufev = NULL;
	}

	if (conn->data.filebuf != NULL)
	{
		evbuffer_free(conn->data.filebuf);
		conn->data.filebuf = NULL;
	}

	if (conn->listener != NULL)
	{
		evconnlistener_free(conn->listener);
		conn->listener = NULL;
	}
}

void free_connection(struct connection *conn)
{
	if (conn->ctrl.bufev != NULL)
		bufferevent_free(conn->ctrl.bufev);

	if (conn->data.bufev != NULL)
		bufferevent_free(conn->data.bufev);

	if (conn->data.filebuf != NULL)
		evbuffer_free(conn->data.filebuf);

	if (conn->listener != NULL)
		evconnlistener_free(conn->listener);

	free(conn);
}

static void cb_signal(int fd, short what, void *arg)
{
	struct event_base *base = arg;

	INFO_OUT("got signal %d\n", fd);

	event_base_loopbreak(base);
}

static void cb_control_read(struct bufferevent *bufev, void *ctx)
{
	struct connection *conn = ctx;
	char cmd[BUFSIZ];
	size_t r;

	if ((r = bufferevent_read(bufev, cmd, BUFSIZ)) > 0)
		parse(conn, cmd, r);
	else
		free_connection(conn);
}

static void cb_control_write(struct bufferevent *bufev, void *ctx)
{
	struct connection *conn = ctx;

	if (conn->ctrl.done && evbuffer_get_length(conn->ctrl.buf))
		free_connection(conn);
}

static void cb_control_event(struct bufferevent *bufev, short what, void *ctx)
{
	struct connection *conn = ctx;

	INFO_OUT("control connection event %d\n", what);

	if (what & BEV_EVENT_EOF || what & BEV_EVENT_TIMEOUT)
	{
		if (what & BEV_EVENT_TIMEOUT)
		{
			INFO_OUT("connection timed out\n");
			send_response(conn->ctrl.bufev, "421 Timeout.\r\n");
			conn->ctrl.done = 1;
		}
		if (what & BEV_EVENT_EOF)
			INFO_OUT("connection closed\n");

		bufferevent_free(bufev);
		free(conn);
	}
}

static void cb_connect(struct evconnlistener *listener, evutil_socket_t sock, struct sockaddr *addr, int len, void *ptr)
{
	struct event_base *base = evconnlistener_get_base(listener);
	struct connection *conn = malloc(sizeof(struct connection));

	conn->base          = base;
	conn->data.bufev    = NULL;
	conn->data.buf      = NULL;
	conn->data.filebuf  = NULL;
	conn->listener      = NULL;
	conn->namelist      = NULL;
	strcpy(conn->cwd, cwd);

	conn->ctrl.bufev = bufferevent_socket_new(base, sock, BEV_OPT_CLOSE_ON_FREE);
	conn->ctrl.buf = bufferevent_get_output(conn->ctrl.bufev);
	conn->ctrl.done = 0;

	if (conn->ctrl.bufev == NULL)
	{
		ERROR_OUT("could not create buffer event\n");
		close(sock);
		free(conn);
		return;
	}

	init_parser(conn);

	/* bufferevent_settimeout(conn->ctrl.bufev, 60, 0); */
	bufferevent_setcb(conn->ctrl.bufev, cb_control_read, cb_control_write, cb_control_event, conn);
	bufferevent_enable(conn->ctrl.bufev, EV_READ | EV_WRITE);

	send_response(conn->ctrl.bufev, "220 uFTPd\r\n");
}

int main(int argc, char **argv)
{
	int sock;
	char option;
	char *addr = NULL;
	int port = 21;
	struct sockaddr_in listen_addr;

	while ((option = getopt(argc, argv, "l:p:h")) != -1)
		switch (option)
		{
			case 'l':
				addr = optarg;
				break;

			case 'p':
				port = atoi(optarg);
				break;

			default:
			case 'h':
				fprintf(option == 'h' ? stdout : stderr,
					"usage: %s [-l address] [-p port]\n", argv[0]);
				return 0;
		}

	memset(&listen_addr, '\0', sizeof(struct sockaddr_in));
	memset(&local_addr, '\0', sizeof(struct sockaddr_in));

	getcwd(cwd, PATH_MAX);

	listen_addr.sin_family = AF_INET;
	listen_addr.sin_addr.s_addr = INADDR_ANY;
	listen_addr.sin_port = htons(port);

	if (addr != NULL)
	{
		if (inet_aton(addr, &listen_addr.sin_addr) == 0)
		{
			ERROR_OUT("invalid listen address");
			return 1;
		}

		local_addr.s_addr = listen_addr.sin_addr.s_addr;
	}
	else
	{
		char hostname[200];
		struct hostent *hostent;

		gethostname(hostname, sizeof(hostname));
		hostent = gethostbyname(hostname);

		if (hostent == NULL)
		{
			ERROR_OUT("could not get retrieve own hostname: %s\n", strerror(errno));
			return 1;
		}

		local_addr.s_addr = ((struct in_addr *)hostent->h_addr_list[0])->s_addr;
	}

	INFO_OUT("listening on %s, port %d\n", inet_ntoa(listen_addr.sin_addr), port);
	INFO_OUT("local address is %s\n", inet_ntoa(local_addr));

	struct event_base *base = event_base_new();

	INFO_OUT("libevent using %s backend\n", event_base_get_method(base));

	struct event *sighup_event = evsignal_new(base, SIGINT, cb_signal, base);
	struct event *sigint_event = evsignal_new(base, SIGHUP, cb_signal, base);
	struct evconnlistener *listener = evconnlistener_new_bind(base, cb_connect,
		NULL, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, BACKLOG,
		(struct sockaddr *)&listen_addr, sizeof(struct sockaddr_in));

	if (listener == NULL)
	{
		fprintf(stderr, "could not listen on port %d\n", port);
		return EXIT_FAILURE;
	}

	evsignal_add(sighup_event, NULL);
	evsignal_add(sigint_event, NULL);

	signal(SIGPIPE, SIG_IGN);

	event_base_dispatch(base);

	event_free(sighup_event);
	event_free(sigint_event);
	evconnlistener_free(listener);

	return EXIT_SUCCESS;
}
